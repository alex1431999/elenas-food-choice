import json
import random
import threading
from channels.generic.websocket import WebsocketConsumer


# Set interval haleper function
def set_interval(func, sec):
    def func_wrapper():
        set_interval(func, sec)
        func()

    t = threading.Timer(sec, func_wrapper)
    t.start()
    return t


class ItemConsumer(WebsocketConsumer):
    messages = [
        "Have you decided yet?",
        "Imagine taking this long!",
        "Just pick anything at this point!",
        "Hurry up Elena!",
        "Jesus Christ...",
        "Nali could literally pick something quicker...",
        "Imagine asking Nali to pick something!",
    ]

    def connect(self):
        self.accept()
        set_interval(self.send_messages, 5)

    def disconnect(self, close_code):
        pass

    def receive(self, text_data=None, bytes_data=None):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        self.send(text_data=json.dumps({
            'message': message
        }))

    def send_messages(self):
        message = random.choice(self.messages)
        self.send(text_data=json.dumps({
            "message": message
        }))

from django.contrib.auth.models import User, Group
from rest_framework import viewsets, permissions
from ElenasFoodChoice.api.serializers import UserSerializer, GroupSerializer, ItemsSerializer
from ElenasFoodChoice.api.models import Item


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]

class ItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Food to be viewed or edited.
    """
    queryset = Item.objects.all()
    serializer_class = ItemsSerializer